INTRODUCTION
------------

Module provides a toggle widget for Boolean field type using the bootstrap
toggle library.

MODULE INSTALLATION
-------------------

Install as you would normally install a Drupal contributed module.

LIBRARY COMPOSER INSTALLATION
-----------------------------

If you use Composer to manage dependencies, edit "/composer.json" as follows.
  1. Run "composer require --prefer-dist composer/installers" to ensure that you
     have the "composer/installers" package installed. This package facilitates
     the installation of packages into directories other than "/vendor" (e.g.
     "/libraries") using Composer.

  2. Add the following to the "installer-paths" section of "composer.json":

  "libraries/{$name}": ["type:drupal-library"],

  or

  "web/libraries/{$name}": ["type:drupal-library"],

  depending on your setup and libraries forlder location.

  3. Add the following to the "repositories" section of "composer.json":

  {
    "type": "package",
    "package": {
      "name": "minhur/bootstrap_toggle",
      "version": "2.2.2",
      "type": "drupal-library",
      "extra": {
        "installer-name": "drupal_toggle"
      },
      "dist": {
        "url": "https://github.com/minhur/bootstrap-toggle/archive/master.zip",
         "type": "zip"
      }
    }
  }

4. Run "composer require minhur/bootstrap_togggle"
- you should find the new directory has been created under "/libraries"


LIBRARY MANUAL INSTALLATION
---------------------------

If you are not managing your project with composer, you
will need to download the required bootstrap toggle library manually.

Download bootstrap toggle library from here:
https://github.com/minhur/bootstrap-toggle/archive/master.zip
(for more information about this library visit this page:
http://www.bootstraptoggle.com)

Extract and place this library under libraries in docroot folder and
rename the extracted folder to 'bootstrap_toggle'.

REQUIREMENTS
------------

This module requires bootstrap theme for all places where you need
to use this toggle button - that means on all add/edit forms.

So make sure you have the bootstrap theme as default theme and don't use
your admin theme for add/edit pages.

CONFIGURATION
-------------

1. Go to manage fields of the content type you wish to have a toggle button
and add a Boolean field.
2. Go to manage form display tab and under widget column select
'Bootstrap Toggle' for your field.
3. Configure the settings for that toggle button and update those settings.
4. Finally save the manage form display and you are done.

Try out different combination of settings to get a different look
of toggle button.

MAINTAINERS
-----------

Current maintainers:
  * Abhishek Pareek (abhishek_pareek) - https://www.drupal.org/user/3231365
  * Jordan Karlov (JordiK) - https://www.drupal.org/user/3472907

This project has been sponsored by:
  * Innoraft Solutions - https://www.drupal.org/node/2145877
